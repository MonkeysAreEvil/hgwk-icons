# Description
Fork of Luv icon theme (https://github.com/Nitrux/luv-icon-theme/).
I've added some of my own app icons (based on official designs), and a couple of other minor status icons.
I occasionally merge new icons from upstream; I prefer some earler version of Luv icons, so keep most unchanged.

# Install

* Copy the directory *hgwk-icons* to `/usr/share/icons` for all users/environments, or `~/.icons` for a GTK user/environment, or `~/.local/share/icons` for a Plasma user/environment.
* Select the theme in the Desktop Environment settings (e.g. `systemsettings5`, `lxappearance`). Alternatively, edit `~/.gtkrc-2.0`, `~/.config/gtk-3.0/settings.ini` etc.
